<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

         DB::table('users')->insert([
          'name' =>'Angel Mendoza',
          'email' =>'angelmendoza@mail.com',
          'password' => bcrypt('123456789'),
          'created_at' => now(),
          'updated_at' => now()
    	]);

         $categories = factory(\App\Group::class, 10)->create(); 
         

    }
}
