import { setAuthorization } from "./general";
import Swal from 'sweetalert2'

export function newCategory(credentials) {
    return new Promise((res, rej) => {
        axios.post('/api/categories/new', credentials)
            .then((response) => {
                setAuthorization(response.data.access_token)
                res(response.data);
            })
            .catch((err) =>{
                rej("Wrong email or password");
            })
    })
}

export function CategoryMessageError() {
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000
    });
    Toast.fire({
      type: 'error',
      title: 'algo salio mal, vuelva a intentarlo mas tarde'
    })
}

export function CategoryMessageSave() {
	Swal.fire({
		type: 'success',
		title: 'Categoria Guardada Satisfactoriamente',
		showConfirmButton: false,
		timer: 1500
	})
}
