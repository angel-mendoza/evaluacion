import Home from './components/Home.vue';
import Login from './components/auth/Login.vue';
import Register from './components/auth/Register.vue';
import CategoriesMain from './components/categories/CategoriesMain.vue';
import CategoriesList from './components/categories/CategoriesList.vue';
import NewCategory from './components/categories/NewCategory.vue';

export const routes = [
	{
		path: '/',
		component: Home,
		meta: {
			requieresAuth: true
		}
	},
	{
		path: '/login',
		component: Login
	},
    {
        path: '/register',
        component: Register,
    },
    {
        path: '/Categories',
        component: CategoriesMain,
        meta: {
            requiresAuth: true
        },
        children: [
            {
                path: '/',
                component: CategoriesList
            },
            {
                path: 'new',
                component: NewCategory
            }
        ]
    }    	
]