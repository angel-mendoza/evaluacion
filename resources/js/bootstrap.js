


window._ = require('lodash');

try {
    window.$ = window.jQuery = require('jquery');
    require( 'datatables.net' );
	require( 'datatables.net-dt' );
    require('bootstrap');
} catch (e) {}


window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';