require('./bootstrap');
require('./classBootstrap');
import Vue from 'vue';
import VueRouter from 'vue-router';
import Vuex from 'vuex';
import {routes} from './routes';
import {initialize} from './helpers/general';
import VueSweetalert2 from 'vue-sweetalert2';

//components
import MainApp from './components/MainApp.vue';

//store
import StoreData from './store'

Vue.use(VueRouter);
Vue.use(Vuex);

const options = {
  confirmButtonColor: '#41b882',
  cancelButtonColor: '#ff7674'
}
 
Vue.use(VueSweetalert2, options)

const store = new Vuex.Store(StoreData)


const router = new VueRouter({
	routes,
	mode: 'history'
});

initialize(store, router)

const app = new Vue({
    el: '#app',
    router,
    store,
    components: {
    	MainApp
    }
});
