<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $fillable = [
        'name', 'descripcion'
    ];

    public function transactions()
    {
    	return $this->hasMany(Transaction::class);
    }
}
