<?php

namespace App\Http\Controllers;

use App\Group;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GroupController extends Controller
{
    public function all()
    {
       $categories = Group::all();

        return response()->json([
            "categories" => $categories
        ], 200);
    }

    public function get()
    {
        $category = Group::whereId($id)->first();

        return response()->json([
            "category" => $category
        ], 200);
    }

    public function new(Request $request)
    {

        $credentials = request(['name' ,'descripcion']);

        $category = new Group();
        $category->name = $credentials['name'];
        $category->descripcion = $credentials['descripcion'];
        $category->save();

        //$category = Group::create($request->only(["name", "descripcion"]));

        return response()->json([
            "category" => $category
        ], 200);
    }

}
