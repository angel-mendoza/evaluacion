<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = [
        'subject', 'amount' , 'type'
    ];

    public function category()
    {
    	return $this->belongsTo(Group::class);
    }
}
